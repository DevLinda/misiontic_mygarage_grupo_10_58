/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygaragegrupo10.mygarage.dao;


import com.mygaragegrupo10.mygarage.models.Usuario;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author netoj
 */
public interface UsuarioDao extends CrudRepository<Usuario, Integer> {
    
}
