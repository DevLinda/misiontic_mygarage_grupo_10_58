/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygaragegrupo10.mygarage.services.imp;

import com.mygaragegrupo10.mygarage.dao.PlazaDao;
import com.mygaragegrupo10.mygarage.models.Plaza;
import com.mygaragegrupo10.mygarage.services.PlazaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlazaServiceImp implements PlazaService{
    
    @Autowired
    PlazaDao plazaDao;

    @Override
    public Plaza save(Plaza plaza) {
        return plazaDao.save(plaza);
    }

    @Override
    public void delete(Integer id) {
        plazaDao.deleteById(id);
    }

    @Override
    public Plaza findById(Integer id) {
        return plazaDao.findById(id).orElse(null);
    }

    @Override
    public List<Plaza> findAll() {
        return (List<Plaza>) plazaDao.findAll();
    }
    
}
