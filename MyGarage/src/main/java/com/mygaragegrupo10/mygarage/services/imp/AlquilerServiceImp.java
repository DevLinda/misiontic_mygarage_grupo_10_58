/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygaragegrupo10.mygarage.services.imp;

import com.mygaragegrupo10.mygarage.dao.AlquilerDao;
import com.mygaragegrupo10.mygarage.models.Alquiler;
import com.mygaragegrupo10.mygarage.services.AlquilerService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Nestor Jose
 */
@Service
public class AlquilerServiceImp implements AlquilerService{
    
    @Autowired
    AlquilerDao alquilerDao;

    @Override
    public Alquiler save(Alquiler alquiler) {
        return alquilerDao.save(alquiler);
    }

    @Override
    public void delete(Integer id) {
        alquilerDao.deleteById(id);
    }

    @Override
    public Alquiler findById(Integer id) {
        return alquilerDao.findById(id).orElse(null);
    }

    @Override
    public List<Alquiler> findAll() {
        return (List<Alquiler>)alquilerDao.findAll();
    }
}
