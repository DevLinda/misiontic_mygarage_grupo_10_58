/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygaragegrupo10.mygarage.services.imp;

import com.mygaragegrupo10.mygarage.dao.VehiculoDao;
import com.mygaragegrupo10.mygarage.models.Vehiculo;
import com.mygaragegrupo10.mygarage.services.VehiculoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Nestor Jose
 */
@Service
public class VehiculoServiceImp implements VehiculoService{
    
    @Autowired
    VehiculoDao vehiculoDao;

    @Override
    public Vehiculo save(Vehiculo vehiculo) {
       return vehiculoDao.save(vehiculo);
    }

    @Override
    public void delete(Integer id) {
        vehiculoDao.deleteById(id);
    }

    @Override
    public Vehiculo findById(Integer id) {
        return vehiculoDao.findById(id).orElse(null);
    }

    @Override
    public List<Vehiculo> findAll() {
        return (List<Vehiculo>) vehiculoDao.findAll();
    }
    
}
