/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygaragegrupo10.mygarage.services.imp;

import com.mygaragegrupo10.mygarage.dao.UsuarioDao;
import com.mygaragegrupo10.mygarage.models.Usuario;
import com.mygaragegrupo10.mygarage.services.UsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Nestor Jose
 */
@Service
public class UsuarioServiceImp implements UsuarioService{
    
    @Autowired
    UsuarioDao usuarioDao;

    @Override
    public Usuario save(Usuario usuario) {
        return usuarioDao.save(usuario);
    }

    @Override
    public void delete(Integer id) {
        usuarioDao.deleteById(id);
    }

    @Override
    public Usuario findById(Integer id) {
        return usuarioDao.findById(id).orElse(null);
    }

    @Override
    public List<Usuario> findAll() {
        return (List<Usuario>) usuarioDao.findAll();
    }
    
}
