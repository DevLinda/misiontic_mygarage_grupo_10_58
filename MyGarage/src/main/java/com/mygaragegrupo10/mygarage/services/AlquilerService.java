/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygaragegrupo10.mygarage.services;

import com.mygaragegrupo10.mygarage.models.Alquiler;
import java.util.List;

/**
 *
 * @author Nestor Jose
 */
public interface AlquilerService {
    public Alquiler save(Alquiler alquiler);
    public void delete(Integer id);
    
    public Alquiler findById(Integer id);
    public List<Alquiler> findAll();
}
