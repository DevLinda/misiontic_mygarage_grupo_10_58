/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygaragegrupo10.mygarage.services;

import com.mygaragegrupo10.mygarage.models.Vehiculo;
import java.util.List;

/**
 *
 * @author Nestor Jose
 */

public interface VehiculoService {
    public Vehiculo save(Vehiculo vehiculo);
    public void delete(Integer id);
    
    public Vehiculo findById(Integer id);
    public List<Vehiculo> findAll();
}
