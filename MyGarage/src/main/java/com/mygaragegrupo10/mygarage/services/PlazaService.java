/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygaragegrupo10.mygarage.services;

import com.mygaragegrupo10.mygarage.models.Plaza;
import java.util.List;

/**
 *
 * @author Nestor Jose
 */
public interface PlazaService {
    public Plaza save(Plaza plaza);
    public void delete(Integer id);
    
    public Plaza findById(Integer id);
    public List<Plaza> findAll();
}
