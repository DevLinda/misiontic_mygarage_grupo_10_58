/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygaragegrupo10.mygarage.services;

import com.mygaragegrupo10.mygarage.models.Usuario;
import java.util.List;

/**
 *
 * @author Nestor Jose
 */
public interface UsuarioService {
    public Usuario save(Usuario usuario);
    public void delete (Integer id);
    
    public Usuario findById(Integer id);
    public List<Usuario> findAll();
}
