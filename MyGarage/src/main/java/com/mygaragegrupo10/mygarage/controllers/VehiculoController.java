/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygaragegrupo10.mygarage.controllers;

import com.mygaragegrupo10.mygarage.models.Vehiculo;
import com.mygaragegrupo10.mygarage.services.VehiculoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Nestor Jose
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/vehiculo")
public class VehiculoController {
    
    @Autowired
    private VehiculoService vehiculoService;
    
    
    @GetMapping(value="/list")
    public List<Vehiculo> consultarVehiculo(){
        return vehiculoService.findAll();
    }
    @GetMapping(value="/list/{id}")
    public Vehiculo consultarporId(@PathVariable Integer id){
        return  vehiculoService.findById(id);
    }
    @PostMapping(value="/")
    public ResponseEntity<Vehiculo> agregar(@RequestBody Vehiculo vehiculo){
        Vehiculo resultado=vehiculoService.save(vehiculo);
        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }
    @PutMapping(value="/")
    public ResponseEntity<Vehiculo> editar(@RequestBody Vehiculo vehiculoedit){
        Vehiculo editado=vehiculoService.findById(vehiculoedit.getIdVehiculo());
        if (editado==null){
            return new ResponseEntity<>(editado,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        else
            editado.setPropietario(vehiculoedit.getPropietario());
            editado.setMarca(vehiculoedit.getMarca());
            editado.setPlaca(vehiculoedit.getPlaca());
            vehiculoService.save(editado);
            return new ResponseEntity<>(editado,HttpStatus.OK);
            
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Vehiculo> eliminar(@PathVariable Integer id){
       Vehiculo resultado=vehiculoService.findById(id);
       if (resultado== null){
           return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);      
       }
       else{
           vehiculoService.delete(id);
           return new ResponseEntity<>(resultado, HttpStatus.OK);
       }
    }
    
}
