/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygaragegrupo10.mygarage.controllers;

import com.mygaragegrupo10.mygarage.models.Alquiler;
import com.mygaragegrupo10.mygarage.services.AlquilerService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Nestor Jose
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/alquiler")
public class AlquilerController {

    @Autowired
    private AlquilerService alquilerService;

    @GetMapping(value = "/list")
    public List<Alquiler> consultarUsuario() {
        return alquilerService.findAll();
    }

    @GetMapping(value = "list/{id}")
    public Alquiler consultarporId(@PathVariable Integer id) {
        return alquilerService.findById(id);
    }

    @PostMapping(value = "/")
    public ResponseEntity<Alquiler> agregar(@RequestBody Alquiler alquiler) {
        Alquiler resultado = alquilerService.save(alquiler);
        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Alquiler> editar(@RequestBody Alquiler alquileredit) {
        Alquiler editado = alquilerService.findById(alquileredit.getIdAlquiler());
        if (editado == null) {
            return new ResponseEntity<>(editado, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            editado.setValor(alquileredit.getValor());
            editado.setHorasDeServicio(alquileredit.getHorasDeServicio());
            return new ResponseEntity<>(editado, HttpStatus.OK);
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Alquiler> eliminar(@PathVariable Integer id) {
        Alquiler resultado = alquilerService.findById(id);
        if (resultado == null) {
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            alquilerService.delete(id);
            return new ResponseEntity<>(resultado, HttpStatus.OK);
        }
    }
}
