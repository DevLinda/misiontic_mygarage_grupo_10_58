/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygaragegrupo10.mygarage.controllers;

import com.mygaragegrupo10.mygarage.models.Usuario;
import com.mygaragegrupo10.mygarage.services.UsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Nestor Jose
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/usuario")
public class UsuarioController {
    
    @Autowired
    private UsuarioService usuarioService;
    
    @GetMapping(value="/list")
    public List<Usuario> consultarUsuario(){
        return usuarioService.findAll();
    }
    
    @GetMapping(value="list/{id}")
    public Usuario consultarporId(@PathVariable Integer id){
        return usuarioService.findById(id);
    }
    
    @PostMapping(value="/")
    public ResponseEntity<Usuario> agregar(@RequestBody Usuario usuario){
        Usuario resultado= usuarioService.save(usuario);
        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }
    @PutMapping(value="/")
    public ResponseEntity<Usuario> editar(@RequestBody Usuario usuarioedit){
        Usuario editado = usuarioService.findById(usuarioedit.getIdUsuario());
        if (editado==null){
            return new ResponseEntity<>(editado, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        else{
            editado.setCargo(usuarioedit.getCargo());
            editado.setNombre(usuarioedit.getNombre());
            return new ResponseEntity<>(editado, HttpStatus.OK);
        }
    }
        @DeleteMapping(value = "/{id}")
    public ResponseEntity<Usuario> eliminar(@PathVariable Integer id) {
        Usuario resultado = usuarioService.findById(id);
        if (resultado == null) {
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            usuarioService.delete(id);
            return new ResponseEntity<>(resultado, HttpStatus.OK);
        }
    }
}
