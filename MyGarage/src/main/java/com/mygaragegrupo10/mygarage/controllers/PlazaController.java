/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygaragegrupo10.mygarage.controllers;

import com.mygaragegrupo10.mygarage.models.Plaza;
import com.mygaragegrupo10.mygarage.services.PlazaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Nestor Jose
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/plaza")
public class PlazaController {

    @Autowired
    private PlazaService plazaService;

    @GetMapping(value = "/list")
    public List<Plaza> consultarPlaza() {
        return plazaService.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public Plaza consultarporId(@PathVariable Integer id) {
        return plazaService.findById(id);
    }

    @PostMapping(value = "/")
    public ResponseEntity<Plaza> agregar(@RequestBody Plaza plaza) {
        Plaza resultado = plazaService.save(plaza);
        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Plaza> editar(@RequestBody Plaza plazaedit) {
        Plaza editado = plazaService.findById(plazaedit.getIdPlaza());
        if (editado == null) {
            return new ResponseEntity<>(editado, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            editado.setArea(plazaedit.getArea());
            editado.setOcupado(plazaedit.getOcupado());
            plazaService.save(editado);
            return new ResponseEntity<>(editado, HttpStatus.OK);
        }

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Plaza> eliminar(@PathVariable Integer id) {
        Plaza resultado = plazaService.findById(id);
        if (resultado == null) {
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            plazaService.delete(id);
            return new ResponseEntity<>(resultado, HttpStatus.OK);
        }
    }
}
