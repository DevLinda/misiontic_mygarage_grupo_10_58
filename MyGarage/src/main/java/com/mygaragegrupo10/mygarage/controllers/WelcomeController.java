/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygaragegrupo10.mygarage.controllers;

import java.awt.BorderLayout;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Nestor Jose
 */
@RestController
@CrossOrigin("*")
public class WelcomeController {
    @RequestMapping("/")
    public String bienvenido(){
    return "<h1>Bienvenido al proyecto grupo-10 MisionTic!!</h1>";
}
}
