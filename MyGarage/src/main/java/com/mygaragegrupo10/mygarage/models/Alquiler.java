/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygaragegrupo10.mygarage.models;

import com.mygaragegrupo10.mygarage.models.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Nestor Jose
 */
@Entity
@Table(name="alquiler")
@Getter
@Setter

public class Alquiler implements Serializable {
    
    @Id
    @GeneratedValue (strategy=GenerationType.IDENTITY)
    @Column(name="idAlquiler")
    private Integer idAlquiler;
    
    @ManyToOne
    @JoinColumn(name="idVehiculo")
    private Vehiculo idVehiculo;
    
    @ManyToOne
    @JoinColumn(name = "idUsuario")
    private Usuario idUsuario;
    
    @ManyToOne
    @JoinColumn(name="idPlaza")
    private Plaza idPlaza;
    
    @Column(name="HorasDeServicio")
    private Integer HorasDeServicio;
    
    @Column(name="Valor")
    private Double Valor;
    
    
}
