/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygaragegrupo10.mygarage.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Nestor Jose
 */
@Entity
@Table(name="vehiculo")
@Getter
@Setter

public class Vehiculo implements Serializable {
    
    @Id
    @GeneratedValue (strategy=GenerationType.IDENTITY)
    @Column(name="idVehiculo")
    private Integer idVehiculo;
    
    @Column(name="Propietario")
    private String Propietario;
    
    @Column(name="Marca")
    private String Marca;
    
    @Column(name="Placa")
    private String Placa;
    
}
